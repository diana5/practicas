'use strict';
 
app.service('CrudService', function($http) {
 
    this.getAll = function() {
        return $http.get('http://localhost:8094/V1/producto').then(function(res){ 

            var d=res.data;        
            return(d); 
        });  

    } 

    this.verCategorias = function() {
        return $http.get('http://localhost:8094/V1/producto/categorias').then(function(res){ 

            var todo=res.data;        
            return(todo); 
        });  

    } 

    this.guardarProduct = function(params){
        return $http.post('http://localhost:8094/V1/producto',JSON.stringify(params)).then(function(res){
            console.log(params)
            var guardados=res.data;        
            return(guardados); 
        }); 
    }
    

});