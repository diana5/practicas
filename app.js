
var app = angular.module('myApp', ['ngRoute']);

app.config(function ($routeProvider) {
  $routeProvider

    .when('/home', { 
      templateUrl: '/index.html',
     activetab: '/home', 
     controller: 'myCtrl' })
     
    .when('/nuevoproducto', {
      templateUrl: 'pages/crearproducto.html',
      controller: 'myCtrl',
      activetab: 'nuevoproducto',
      
    })
    .otherwise({ redirectTo: '/home' });
});


//  MyCtrl controller



app.controller('myCtrl', function($scope, CrudService, OtroService) {


  $scope.prueba=function(){
  CrudService.getAll().then(function(successResponse){
    $scope.array = successResponse;
    console.log($scope.array);
  });

   }

   $scope.prueba();
  
   $scope.verCategories=function(){
    CrudService.verCategorias().then(function(successResponse){
      $scope.categorias = successResponse;
      console.log($scope.categorias);
    });
  
     }
     $scope.verCategories();

     $scope.guardarProducto=function(nombre,desc,cat,cant,fecha){
      var  params = {
        nombre : nombre,
        descripcion :desc,
        categoria:{id:cat},
        cantidad :cant,
        fecha : fecha
      };
      CrudService.guardarProduct(params).then(function(successResponse){
      $scope.guardado = successResponse;
      console.log($scope.guardado)
    }) };

     //el otro service

     $scope.mostrarTodo=function(){
      OtroService.mostrar().then(function(successResponse){
        $scope.mostrarT = successResponse;
        console.log($scope.mostrarT);
      });
    
       }

  

    

      

});



